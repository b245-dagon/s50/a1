import AppNavbar from './Components/AppNavbar';
import './App.css';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Notfound from './pages/404Page';
import { Navigate } from 'react-router-dom';
import {BrowserRouter as Router, Routes , Route} from 'react-router-dom'
import  {UserProvider}  from './UserContext';
import { useState , useEffect } from 'react';
import CourseView from './Components/CourseView';
function App() {

    const [user , setUser] = useState(null);

    useEffect(()=>{
      console.log(user)
    },[user])
    
    const unSetUser = () =>{
      localStorage.clear()
}

useEffect(()=>{

  fetch("http://localhost:3001/user/details",{
    headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
    }
 })
 .then(result => 
    result.json()
 ).then(data => {
    console.log(data)
    if(localStorage.getItem('token')!== null){
      setUser({
        id:data._id,
        isAdmin: data.isAdmin
    })
    }



  
 })


},[])
  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
      <AppNavbar/>
      <Routes>
      <Route path="/" element = {<Home/>}/>
      <Route path="/courses" element = {<Courses/>}/>
      <Route path="/login" element = {<Login/>}/>
      <Route path="/register" element = {<Register/>}/>
      <Route path="/logout" element = {<Logout/>}/>
      <Route path="/Not-found" element = {<Notfound/>}/>
      <Route path="*" element = {<Navigate to = "Not-found"/>}/>
      <Route path="/course/:courseId" element = {<CourseView/>}/>

      
    </Routes>
    
    </Router>
    </UserProvider>
  )
    
}

export default App;
