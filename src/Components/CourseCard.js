
import { useEffect, useState, useContext } from 'react';
import {Row , Col} from 'react-bootstrap'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Course ({courseProp}) {

    const { _id ,name, description ,price} = courseProp;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components

        //const [getter , setter ] = useStates(initialGetterValue)

        // ENROLLEESS SECTION
        const [enrollees, setEnrollees] = useState(0);
        const [availableSeats , setAvailableSeasts] = useState(30)



        const[isDisabled, setIsDisabled] = useState(false)

        useEffect(()=>{
            if(availableSeats === 0){
                setIsDisabled(true);
            }
        },[availableSeats])

        // // setEnrollees(1)
        // console.log(setEnrollees)

        const{user}= useContext(UserContext)

        function enroll(){

            setEnrollees(enrollees+1)
            setAvailableSeasts(availableSeats-1)
                if(enrollees >=29){
                    alert("Congrats for making it to the cut!")
                    setEnrollees(30)
                    setAvailableSeasts(0)
                   }
            }
       
        

        

    return (
       
        <Row className='mt-3'>
            <Col>
                <Card>
                <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
                <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{enrollees}</Card.Text>
                <Card.Subtitle>Available Seats:</Card.Subtitle>
                <Card.Text>{availableSeats}</Card.Text>

                {
                    user?
                    <Button as={Link} to = {`/course/${_id}`} disabled ={isDisabled}>See More Details</Button>
                    :
                    <Button as={Link} to ="/login" id="onclick">Login</Button>
                }
              
                </Card.Body>
                </Card>
            </Col>
        </Row>
       

    )
}