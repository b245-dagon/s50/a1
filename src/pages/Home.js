import { Fragment } from "react"
import Banner from "../Components/Banner"
import Highlights from "../Components/Highlights"



export default function Home (){
    return (
        <Fragment>
            <Banner/>
            <Highlights/>
          
           
        </Fragment>
    )
}