import {Form , Button} from 'react-bootstrap';
import { Fragment } from 'react';
import {  useState ,useEffect, useContext} from 'react';
import { Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'


export default function Login(){

    // Login useState

    const [email , setEmail] = useState('');
    const [password , setPassword] = useState('');

    const navigate = useNavigate()


    // Allows us to consume the UserContext object and its properties for user validation

    const {user, setUser} = useContext(UserContext)
    console.log(user)

    // Button useState
    const [isActive , setIsActive] = useState(false)

    useEffect(()=>{
        if(email !== "" && password !== ""){
            setIsActive(true)
        }else{
            setIsActive(false)
        }

    },[email,password])

    function login(event){
        event.preventDefault();


        //Process a fetch request to corresponding back end API
        fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
            method: 'POST',
            headers:{
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })

        }).then(result => result.json())
        .then(data => {
            console.log(data)

            if(data === false){

                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "Please try again"
                })
               
            }else{
                localStorage.setItem('token',data.auth);
                retrieveUserDetails(localStorage.getItem('token'));
                
                Swal.fire({
                    title: "Authentication successful",
                    icon: 'success',
                    text: "Welcome to Zuitt"
                })
                navigate("/");
                
            }
        })


        // localStorage.setItem("email", email);
        // setUser(localStorage.getItem("email"))
        // alert("You are now logged in")
        // setEmail('');
        // setPassword('');
        
       
    }

    const retrieveUserDetails = (token) =>{
         // the token sent as part of the request; header information
         fetch("http://localhost:3001/user/details",{
            headers:{
                Authorization: `Bearer ${token}`
            }
         })
         .then(result => 
            result.json()
         ).then(data => {
            console.log(data)

            setUser({
                id:data._id,
                isAdmin: data.isAdmin
            })
         })
         
    }

    return(
        user ?
        <Navigate to = "*"/>
     
        
        :
    <Fragment>
    <h1 className='text-center'>Login</h1>
    <Form className='mt-5' onSubmit={event => login(event)}>
      <Form.Group className="mb-3" controlId="formGroupEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email" 
        value = {email}
        onChange ={event => setEmail(event.target.value)} 
        required

        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formGroupPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Password"
        value = {password}
        onChange ={event => setPassword(event.target.value)} 
        required />
      </Form.Group>
            {
                isActive?
                <Button variant="primary" type="submit">
                Submit
                 </Button>
                 :
                 <Button variant="dark" type="submit" disabled>
                Submit
                 </Button>
            }
        </Form>
        </Fragment>
    )
}