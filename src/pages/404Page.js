
import { Fragment } from "react"
import { Link } from "react-router-dom"


export default function Notfound(){

    return(
        <Fragment>
        <h1 className="text-center">404 ERROR </h1>
        <h1 className="text-center">page not Found!</h1>
    
        <p className="text-center">Go back to <Link to= "/" >homepage</Link></p>
     
        </Fragment>
        

    )

}